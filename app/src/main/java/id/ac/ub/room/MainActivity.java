package id.ac.ub.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button bt1;
    Button bt2;
    EditText et1;
    RecyclerView rv1;
    private AppDatabase appDb;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDb = AppDatabase.getInstance(getApplicationContext());
        Adapter Adapter;
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        rv1 = findViewById(R.id.rv1);
        et1 = findViewById(R.id.et1);

        rv1.setLayoutManager(new LinearLayoutManager(this));
        Adapter = new Adapter();
        rv1.setAdapter(Adapter);
        rv1.addItemDecoration(new ItemSpacingDecoration(8));

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i++;
                Item item = new Item();
                item.setJudul(et1.getText().toString());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        appDb.itemDao().insertAll(item);
                    }
                });
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        List<Item> list = appDb.itemDao().getAll();
                        Adapter.setData(list);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Adapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
            }
        });
    }
}